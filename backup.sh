#!/bin/bash

cd server
git add .
git commit -m "server backup at $(date +'%x %T%z')"
git push origin --mirror
